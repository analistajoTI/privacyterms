package com.johannaortiz.spring.security.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * Clase que me permite conectarme a la base de datos seleccionada en SQL Server
 *
 * @author Fabian Andres Caicedo Cuellar
 *
 */
public class ReactDB {

	/**
	 * IP del servidor de la base de datos
	 */
	public static String IP = "192.168.99.102";

	/**
	 * Puerto de conexión a la base de datos
	 */
	public static String PORT = "1433";

	/**
	 * Nombre de base de datos a la que te vas a conectar
	 */
	public static String DATABASENAME = "REACTPRUEBA";

	/**
	 * Usuario para conectarse a la base de datos
	 */
	public static String USER = "sa";

	/**
	 * Contraseña del usuario con el que te vas a la base de datos
	 */
	public static String PASSWORD = "reallyStrongPwd123";

	/**
	 * Abre conexion con la base de datos SQL Server
	 *
	 * @return Conexion a la base de datos de acuerdo al esquema
	 * @throws SQLException
	 */
	private static Connection connection() throws SQLException, ClassNotFoundException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection connection = null;
		if (DATABASENAME != null) {
			connection = DriverManager.getConnection("jdbc:sqlserver://" + IP + ":" + PORT + ";databaseName="
					+ DATABASENAME + ";user=" + USER + ";password=" + PASSWORD);
		} else {
			connection = DriverManager
					.getConnection("jdbc:sqlserver://" + IP + ":" + PORT + ";user=" + USER + ";password=" + PASSWORD);
		}
		return connection;
	}

	public static void updateUser(Long id) throws SQLException, ClassNotFoundException {
		Connection con = connection();
		String sql = "update users set update_user = 0 where id = ?;";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, id);
		int rowsAffected = ps.executeUpdate();
		if (rowsAffected == 0) {
			throw new SQLException("Update ok");
		}
		con.commit();
		ps.close();
		con.close();
	}

}