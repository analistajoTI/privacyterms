package com.johanaortiz.spring.security.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "privacy_terms",
	uniqueConstraints = { 
	@UniqueConstraint(columnNames = "idPerson")})
public class PrivacyTerms {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	private Long idPerson;
	
	@Column
	private String name;
	
	@Column
	private String lastName;
	
	@Column
	private String typeIdentification;
	
	@Column
	private String identification;
	
	@Column
	private Boolean check1;

	@Column
	private Boolean check2;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(Long idPerson) {
		this.idPerson = idPerson;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTypeIdentification() {
		return typeIdentification;
	}

	public void setTypeIdentification(String typeIdentification) {
		this.typeIdentification = typeIdentification;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public Boolean getCheck1() {
		return check1;
	}

	public void setCheck1(Boolean check1) {
		this.check1 = check1;
	}

	public Boolean getCheck2() {
		return check2;
	}

	public void setCheck2(Boolean check2) {
		this.check2 = check2;
	}
	
	

}
