package com.johanaortiz.spring.security.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.johanaortiz.spring.security.models.PrivacyTerms;

public interface PrivacyTermsRepository extends CrudRepository<PrivacyTerms, String> {
	
}
