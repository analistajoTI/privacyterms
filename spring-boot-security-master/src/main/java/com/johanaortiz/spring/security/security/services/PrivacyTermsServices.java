package com.johanaortiz.spring.security.security.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.johanaortiz.spring.security.models.PrivacyTerms;
import com.johanaortiz.spring.security.repository.PrivacyTermsRepository;


@Service
public class PrivacyTermsServices {
	@Autowired
	private PrivacyTermsRepository serv;
	
	public List<PrivacyTerms>find_all(){
		return (List<PrivacyTerms>) serv.findAll();
	}
	
	
	public void agregar(PrivacyTerms entidad) {
		serv.save(entidad);
		
	}
	
	public void actualizar(PrivacyTerms entidad) {
		serv.save(entidad);
		
	}
	
	public void borrar(PrivacyTerms entidad) {
		serv.delete(entidad);
		
	}

}
