package com.johanaortiz.spring.security.security.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.johanaortiz.spring.security.models.Persona;
import com.johanaortiz.spring.security.repository.PersonaRepository;



@Service
public class PersonaServices {
	@Autowired
	private PersonaRepository serv;
	
	public List<Persona>find_all(){
		return (List<Persona>) serv.findAll();
	}
	
	public void agregar(Persona entidad) {
		serv.save(entidad);
		
	}
	
	public void actualizar(Persona entidad) {
		serv.save(entidad);
		
	}
	
	public void borrar(Persona entidad) {
		serv.delete(entidad);
		
	}

}
