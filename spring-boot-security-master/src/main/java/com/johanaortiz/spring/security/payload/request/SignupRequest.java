package com.johanaortiz.spring.security.payload.request;

import java.util.Set;

import javax.validation.constraints.*;
 
public class SignupRequest {
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
    
    private Boolean check1;
    
    private Boolean check2;
    
    private Boolean updateUser;

    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Set<String> getRole() {
      return this.role;
    }
    
    public void setRole(Set<String> role) {
      this.role = role;
    }

	public Boolean getCheck1() {
		return check1;
	}

	public void setCheck1(Boolean check1) {
		this.check1 = check1;
	}

	public Boolean getCheck2() {
		return check2;
	}

	public void setCheck2(Boolean check2) {
		this.check2 = check2;
	}

	public Boolean getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Boolean updateUser) {
		this.updateUser = updateUser;
	}
	
	
    
}
