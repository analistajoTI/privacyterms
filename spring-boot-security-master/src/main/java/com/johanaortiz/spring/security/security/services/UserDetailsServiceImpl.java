package com.johanaortiz.spring.security.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.johanaortiz.spring.security.models.User;
import com.johanaortiz.spring.security.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  UserRepository userRepository;

@Override
public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
	// TODO Auto-generated method stub
	User user = userRepository.findByEmail(email)
	        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + email));

	    return UserDetailsImpl.build(user);

}


}
