package com.johanaortiz.spring.security.payload.response;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;
	private Boolean check1;
	private Boolean check2;
	private Boolean updateUser;

	public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles,Boolean check1, Boolean check2, Boolean updateUser) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.check1 = check1;
		this.check2 = check2;
		this.updateUser = updateUser;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}

	public Boolean getCheck1() {
		return check1;
	}

	public void setCheck1(Boolean check1) {
		this.check1 = check1;
	}

	public Boolean getCheck2() {
		return check2;
	}

	public void setCheck2(Boolean check2) {
		this.check2 = check2;
	}

	public Boolean getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Boolean updateUser) {
		this.updateUser = updateUser;
	}
	
	
}
