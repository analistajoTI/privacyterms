package com.johanaortiz.spring.security.controllers;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.johanaortiz.spring.security.models.PrivacyTerms;
import com.johanaortiz.spring.security.security.services.PrivacyTermsServices;
import com.johannaortiz.spring.security.db.ReactDB;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/privacyterms/")
public class PrivacyTermsController {
	
	@Autowired
	private PrivacyTermsServices serv;

	@GetMapping(value = "/all")
	public List<PrivacyTerms> getAll() {
		return serv.find_all();
	}
	
	@GetMapping(value = "/getById")
	public PrivacyTerms getById(@RequestParam("idPerson") Long idPerson) {
		List<PrivacyTerms> list = serv.find_all();
		PrivacyTerms pterms = new PrivacyTerms();
		for (PrivacyTerms privacyTerms : list) {
			if(privacyTerms.getIdPerson() == idPerson) {
				pterms = privacyTerms;
				pterms.setCheck1(false);
				pterms.setCheck2(false);
			}
		}
		pterms.setIdPerson(idPerson);
		return pterms;
	}
	
	@GetMapping(value = "/updateuser")
	public void updateuser(@RequestParam("id") Long id) {
		try {
			ReactDB.updateUser(id);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, path ="/save")
	public void insertar(@RequestBody PrivacyTerms entidad) {
		serv.agregar(entidad);
	}
	
	@RequestMapping(method = RequestMethod.POST, path ="/delete")
	public void eliminar(@RequestBody PrivacyTerms entidad) {
		serv.borrar(entidad);
	}
	
}
