package com.johanaortiz.spring.security.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.johanaortiz.spring.security.models.Persona;
import com.johanaortiz.spring.security.security.services.PersonaServices;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class PersonaController {
	
	@Autowired
	private PersonaServices serv;

	@GetMapping(value = "/all")
	public List<Persona> getAll() {
		return serv.find_all();
	}

	@RequestMapping(method = RequestMethod.POST, path ="/save")
	public void insertar(@RequestBody Persona entidad) {
		serv.agregar(entidad);
	}
	
	@RequestMapping(method = RequestMethod.POST, path ="/delete")
	public void eliminar(@RequestBody Persona entidad) {
		serv.borrar(entidad);
	}
	
}
