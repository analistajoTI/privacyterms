package com.johanaortiz.spring.security.repository;

import org.springframework.data.repository.CrudRepository;

import com.johanaortiz.spring.security.models.Persona;


public interface PersonaRepository extends CrudRepository<Persona, String> {
		
}
