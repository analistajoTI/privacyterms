import axios from "axios";


const API_URL = "http://localhost:8080/spring-boot-security-0.0.1-SNAPSHOT/api/auth/";

class AuthService {
  
  login(email, password) {
    return axios
      .post(API_URL + "signin", {
        email,
        password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem("email", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.clear();
  }

  register(email, password, check1, check2, updateUser) {
    return axios.post(API_URL + "signup", {
      email,
      password,
      check1,
      check2,
      updateUser
    });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem("email"));
  }

  getCurrentIdUser() {
    return JSON.parse(localStorage.getItem("id"));
  }
}

export default AuthService;