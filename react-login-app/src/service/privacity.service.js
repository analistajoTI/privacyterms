import axios from 'axios';

class privacityService{
    baseUrl = "http://localhost:8080/spring-boot-security-0.0.1-SNAPSHOT/privacyterms/";
    getAll(){
        return axios.get(this.baseUrl + "all").then(res => res.data);
    }
    getByIdPerson(idperson){
        return axios.get(this.baseUrl + "getById?idPerson="+idperson).then(res => res.data);
    }
    getByUpdate(id){
        return axios.get(this.baseUrl + "updateuser?id="+id).then(res => res.data);
    }
    save(privacyterms){
        return axios.post(this.baseUrl + "save",privacyterms).then(res =>res.data);
    }
    delete(privacyterms){
        return axios.post(this.baseUrl + "delete",privacyterms).then(res => res.data);
    }
}

export default privacityService;