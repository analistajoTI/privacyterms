import axios from 'axios';

class PersonaService{
    baseUrl = "http://localhost:8080/spring-boot-security-0.0.1-SNAPSHOT/api/v1/";
    getAll(){
        return axios.get(this.baseUrl + "all").then(res => res.data);
    }
    save(persona){
        return axios.post(this.baseUrl + "save",persona).then(res =>res.data);
    }
    delete(persona){
        return axios.post(this.baseUrl + "delete",persona).then(res => res.data);
    }
}

export default PersonaService;