import React, { Component, useState } from 'react';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { Dropdown } from 'primereact/dropdown';
import { InputSwitch } from 'primereact/inputswitch';
import CheckButton from "react-validation/build/button";
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import authService from '../service/auth.service';
import privacityService from '../service/privacity.service';

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Campo requerido!
      </div>
    );
  }
};

const typesId = [
  { label: 'Cedula', value: 'C.C' },
  { label: 'Tarjeta de Identidad', value: 'T.I' },
  { label: 'NIT', value: 'NIT' },
  { label: 'Pasaporte', value: 'PS' },
];

export default class form extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.privacityService = new privacityService();
    this.authService = new authService();
    this.state = {
      successful: false,
      message: "",
      user2: null,
      privacyterms: {
        id: null,
        idPerson: null,
        name: null,
        lastName: null,
        typeIdentification: null,
        identification: null,
        check1: false,
        check2: false
      }
    };
  }

  handleRegister(e) {
    e.preventDefault();
    this.setState({
      message: "",
      successful: false
    });

    const user2 = this.authService.getCurrentUser();

    if (this.state.privacyterms.check1 && this.state.privacyterms.check2) {
      this.privacityService.save(this.state.privacyterms).then(
        response => {
          this.setState({
            successful: true
          });
          this.privacityService.getByUpdate(user2.id);
          this.authService.logout();
          window.location.assign("https://www.johannaortiz.com/");
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            successful: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        successful: false,
        message: "Acepte las politicas de privacidad para continuar!"
      });
    }

  }

  componentDidMount() {
    const user1 = this.authService.getCurrentUser();
    if (!user1.updateUser) {
      window.location.assign("https://www.johannaortiz.com/");
    }
    this.privacityService.getByIdPerson(user1.id).then(data => this.setState({ privacyterms: data }))
  }

  render() {
    return (
      <Form
        onSubmit={this.handleRegister}
        ref={c => {
          this.form = c;
        }}
      >
        {!this.state.successful && (
          <div>
          <div className="card card-container1">
            <div><h3><center>TÉRMINOS DE PRIVACIDAD</center></h3></div>
            <div className="form-group">
              <div className="form-group" style={{ width: '100%' }}>
                <label htmlFor="names">Nombres: </label>
                <Input
                  type="text"
                  className="form-control"
                  name="names"
                  value={this.state.privacyterms.name}
                  onChange={(e) => {
                    let val = e.target.value;
                    this.setState(prevState => {
                      let privacyterms = Object.assign({}, prevState.privacyterms);
                      privacyterms.name = val.toUpperCase();
                      return { privacyterms };
                    })
                  }}
                />

              </div>
              <div className="form-group" style={{ width: '100%' }}>
                <label htmlFor="lastnames">Apellidos: </label>
                <Input
                  type="text"
                  className="form-control"
                  name="lastnames"
                  value={this.state.privacyterms.lastName}
                  onChange={(e) => {
                    let val = e.target.value;
                    this.setState(prevState => {
                      let privacyterms = Object.assign({}, prevState.privacyterms);
                      privacyterms.lastName = val;
                      return { privacyterms };
                    })
                  }}
                />
              </div>
              <div className="form-group" style={{ width: '5px', height: '5px' }}></div>

              <div className="form-group" style={{ width: '100%' }}>
                <Dropdown style={{ width: '100%' }} value={this.state.privacyterms.typeIdentification} options={typesId} onChange={(e) => {
                  let val = e.target.value;
                  this.setState(prevState => {
                    let privacyterms = Object.assign({}, prevState.privacyterms);
                    privacyterms.typeIdentification = val;
                    return { privacyterms };
                  })
                }} placeholder="Tipo Identificacion" />
              </div>

              <div className="form-group" style={{ width: '100%' }}>
                <label htmlFor="idNumber">Numero de Identificacion: </label>
                <Input
                  type="text"
                  className="form-control"
                  name="idNumber"
                  value={this.state.privacyterms.identification}
                  onChange={(e) => {
                    let val = e.target.value;
                    this.setState(prevState => {
                      let privacyterms = Object.assign({}, prevState.privacyterms);
                      privacyterms.identification = val;
                      return { privacyterms };
                    })
                  }}
                />
              </div>

              <div className="form-group" style={{ width: '5px' }}></div>
              <InputSwitch checked={this.state.privacyterms.check1} onChange={(e) => {
                let val = e.target.value;
                this.setState(prevState => {
                  let privacyterms = Object.assign({}, prevState.privacyterms);
                  privacyterms.check1 = val;
                  return { privacyterms };
                })
              }} />
              <div className="form-group" style={{ width: '5px' }}></div>
              <InputSwitch checked={this.state.privacyterms.check2} onChange={(e) => {
                let val = e.target.value;
                this.setState(prevState => {
                  let privacyterms = Object.assign({}, prevState.privacyterms);
                  privacyterms.check2 = val;
                  return { privacyterms };
                })
              }} />
              <center>
                <div className="form-group" style={{ width: '30%', align: "center" }}>
                  <button className="btn btn-primary btn-block" >Enviar</button>
                </div>
              </center>
              {this.state.message && (
                <div className="form-group">
                  <div
                    className={
                      this.state.successful
                        ? "alert alert-success"
                        : "alert alert-danger"
                    }
                    role="alert"
                  >
                    {this.state.message}
                  </div>
                </div>
              )}
            </div>
            <div className="form-group" style={{ width: '10px' }}></div>
          </div>
          <div className="card card-container1">
            <div className="form-group">
              <h4>Resumen de las políticas de privacidad</h4>
              La DPTI - Tiene como razón fundamental, velar por la prevención de los accidentes de trabajo y las enfermedades profesionales originadas en el trabajo; así como contribuir al bienestar físico y mental de sus empleados, para esto la Corporación Interuniversitaria de Servicios se apoya en los siguientes principios:

              Cumplimiento de todas las normas legales vigentes en el país sobre Prevención de Riesgos Laborales.
              Protección y mantenimiento del mayor nivel de bienestar físico y mental de todos los trabajadores, a través de actividades de promoción y prevención, buscando minimizar los accidentes de trabajo y enfermedades profesionales.
            </div>
            </div>
          </div>
        )}
        <CheckButton
          style={{ display: "none" }}
          ref={c => {
            this.checkBtn = c;
          }}
        />
      </Form>
    );
  }

}