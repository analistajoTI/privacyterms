import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import { InputSwitch } from 'primereact/inputswitch';

import AuthService from "../service/auth.service";

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Campo requerido!
      </div>
    );
  }
};

const email = value => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        No es un email valido!
      </div>
    );
  }
};

const vpassword = value => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        El password debe tener entre 6 y 40 caracteres.
      </div>
    );
  }
};

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeCheck1 = this.onChangeCheck1.bind(this);
    this.onChangeCheck2 = this.onChangeCheck2.bind(this);
    this.AuthService = new AuthService();

    this.state = {
      email: "",
      password: "",
      check1: false,
      check2: false,
      updateUser: true,
      successful: false,
      message: ""
    };
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }
  onChangeCheck1(e) {
    this.setState({
      check1: e.target.value
    });
  }

  onChangeCheck2(e) {
    this.setState({
      check2: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      successful: false
    });

    this.form.validateAll();
    if (this.state.check1 && this.state.check2) {
      if (this.checkBtn.context._errors.length === 0) {
        this.AuthService.register(
          this.state.email,
          this.state.password,
          this.state.check1,
          this.state.check2,
          this.state.updateUser
        ).then(
          response => {
            this.setState({
              message: response.data.message,
              successful: true
            });
          },
          error => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();

            this.setState({
              successful: false,
              message: resMessage
            });
          }
        );
      }
    } else {
      this.setState({
        successful: false,
        message: "Debe aceptar las politicas e privacidad para continuar!"
      })
    }
    }
    render() {
      return (
        <div className="col-md-12">
          <div className="card card-container">
            <img
              src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
              alt="profile-img"
              className="profile-img-card"
            />

            <Form
              onSubmit={this.handleRegister}
              ref={c => {
                this.form = c;
              }}
            >
              {!this.state.successful && (
                <div>
                  <div><h3><center>SingUp</center></h3></div>
                  <div className="form-group">
                    <label htmlFor="email">Email: </label>
                    <Input
                      type="text"
                      className="form-control"
                      name="email"
                      value={this.state.email}
                      onChange={this.onChangeEmail}
                      validations={[required, email]}
                    />
                  </div>

                  <div className="form-group">
                    <label htmlFor="password">Contraseña: </label>
                    <Input
                      type="password"
                      className="form-control"
                      name="password"
                      value={this.state.password}
                      onChange={this.onChangePassword}
                      validations={[required, vpassword]}
                    />
                  </div>

                  <div className="form-group" style={{ width: '5px' }}></div>
                  <InputSwitch checked={this.state.check1}
                    onChange={this.onChangeCheck1}
                  />
                  <div className="form-group" style={{ width: '5px' }}></div>
                  <InputSwitch checked={this.state.check2}
                    onChange={this.onChangeCheck2}
                  />

                  <div className="form-group">
                    <button className="btn btn-primary btn-block">Sign Up</button>
                  </div>
                </div>
              )}

              {this.state.message && (
                <div className="form-group">
                  <div
                    className={
                      this.state.successful
                        ? "alert alert-success"
                        : "alert alert-danger"
                    }
                    role="alert"
                  >
                    {this.state.message}
                  </div>
                </div>
              )}
              <CheckButton
                style={{ display: "none" }}
                ref={c => {
                  this.checkBtn = c;
                }}
              />
            </Form>
          </div>
        </div>
      );
    }
  }
